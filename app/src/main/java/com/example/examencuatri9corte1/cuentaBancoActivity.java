package com.example.examencuatri9corte1;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class cuentaBancoActivity extends AppCompatActivity {

    private EditText txtnumCuenta, txtNombre, txtBanco, txtSaldo, txtCantidad;
    private Spinner spinner;
    private TextView lblSaldo;
    private Button btnAplicar, btnRegresar, btnLimpiar;
    private CuentaBanco cuenta = new CuentaBanco();
    private int pos=0;
    private boolean primer=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cuenta_banco);

        iniciarComponentes();
        btnAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtnumCuenta.getText().toString().matches("")
                ||txtNombre.getText().toString().matches("")||
                txtBanco.getText().toString().matches("")
                ||txtSaldo.getText().toString().matches("")){
                    Toast.makeText(cuentaBancoActivity.this,"Hay datos faltantes",Toast.LENGTH_SHORT).show();
                }else {
                    if (!primer){
                        float saldo = Float.parseFloat(txtSaldo.getText().toString());
                        cuenta.setSaldo(saldo);
                        primer=true;
                    }
                    float cantidad = Float.parseFloat(txtCantidad.getText().toString());

                    switch (pos){
                        case 0:
                            lblSaldo.setText("Saldo Actual: "+ cuenta.obtenerSaldo());
                        break;
                        case 1:
                            if (txtCantidad.getText().toString().matches("")){
                                Toast.makeText(cuentaBancoActivity.this, "Favor de Ingresar una Cantidad",Toast.LENGTH_SHORT).show();
                            }else{
                                if(cantidad>cuenta.getSaldo()){
                                    Toast.makeText(cuentaBancoActivity.this, "No hay saldo suficiente",Toast.LENGTH_SHORT).show();
                                } else{
                                    cuenta.retirarDinero(cantidad);
                                    lblSaldo.setText("Nuevo Saldo: "+ cuenta.obtenerSaldo());
                                }
                            }
                        break;
                        case 2:
                            if (txtCantidad.getText().toString().matches("")){
                                Toast.makeText(cuentaBancoActivity.this, "Favor de Ingresar una Cantidad",Toast.LENGTH_SHORT).show();
                            }else{
                                cuenta.hacerDeposito(cantidad);
                                lblSaldo.setText("Nuevo Saldo: "+ cuenta.obtenerSaldo());
                            }
                        break;
                    }
                }
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                pos=i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtnumCuenta.setText("");
                txtSaldo.setText("");
                txtBanco.setText("");
                txtNombre.setText("");
                txtCantidad.setText("");
                spinner.setSelection(0);
                lblSaldo.setText("Nuevo Saldo: ");
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes(){
        txtnumCuenta = (EditText) findViewById(R.id.txtnumCuenta);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtBanco = (EditText) findViewById(R.id.txtBanco);
        txtSaldo = (EditText) findViewById(R.id.txtSaldo);
        lblSaldo = (TextView) findViewById(R.id.lblSaldo);
        btnAplicar = (Button) findViewById(R.id.btnAplicar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        spinner = (Spinner) findViewById(R.id.spnMovimientos);
        txtCantidad=(EditText) findViewById(R.id.txtCantidad);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.movimientos));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
    }
}