package com.example.examencuatri9corte1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private EditText txtUsuario, txtPass;
    private Button  btnEntrar, btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);

        iniciarComponentes();

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtPass.getText().toString().matches("")
                ||txtUsuario.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this,"Favor de llenar los datos",Toast.LENGTH_SHORT).show();
                }else {
                    if (txtUsuario.getText().toString().matches("admin")&&
                    txtPass.getText().toString().matches("1234")){
                        Intent intent = new Intent(MainActivity.this, cuentaBancoActivity.class);
                        startActivity(intent);
                    }else {
                        Toast.makeText(MainActivity.this, "Usuario y/o contraseña incorrectos",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes(){
        txtPass = (EditText) findViewById(R.id.txtPass);
        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnSalir = (Button) findViewById(R.id.btnSalir);
    }
}